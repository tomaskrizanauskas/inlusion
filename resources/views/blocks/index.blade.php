@extends("layouts.app")
@section("content")

<div class="container">
	<div class="row">
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif

		<h1>Blocks</h1>                                                                                   
		<div class="table-responsive">          
			<table class="table">
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Type</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@foreach($blocks as $block)
						<tr>
							<td>{{ $block->id }}</td>
							<td>{{ $block->name }}</td>
							<td>{{ $block->type->name }}</td>
							<td>
								<a class="btn btn-primary" href="{{ route('blocks.edit', $block->id) }}">Edit</a>
								{{ Form::open(['route' => ['blocks.destroy', $block->id], 'method' => 'POST', 'class' => 'inline']) }}
								{{ Form::hidden('_method', 'DELETE') }}
								{{ form::submit('Delete', ['class'=>'btn btn-danger ']) }}
								{{ Form::close() }}
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>	

		<div class="form-inline form-group">
			<input type="text" id="block-name" class="form-control" name="name" placeholder="Name">
			{{Form::select('type_id', $selectTypes, '',['class'=>'form-control', 'id' => 'type-id']) }}
			<button class="btn btn-success" id="block-add">Add</button>
		</div>

	</div>
</div>

@endsection