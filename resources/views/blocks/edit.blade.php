@extends("layouts.app")
@section("content")

<div class="container">
	
	<div class="row">
		@if (count($errors) > 0)
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif

		<h1>Edit Block</h1>

		{{ Form::open(['route' => ["blocks.update", $block->id], 'method' => 'POST'] ) }}
		{{ Form::hidden('_method', 'PUT') }}
		

		{{ Form::label('name', 'Name:')}}
		{{ Form::text('name', $block->name, ['class'=>'form-control']) }}

		<hr>

		{{ form::submit('Save', ['class'=>'btn btn-primary spacebtn']) }}
		{{ Form::close() }}

		{{ Form::close() }}

	</div>
</div>
@endsection