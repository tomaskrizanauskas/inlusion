@extends("layouts.app")
@section("content")

<div class="container">
	<div class="row">
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		
		<h1>Fields of type "{{ $type->name }}"</h1>                                                                                   
		<div class="table-responsive">          
			<table class="table">
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@foreach($fields as $field)
						<tr>
							<td>{{ $field->id }}</td>
							<td>{{ $field->name }}</td>
							<td>
								{{ Form::open(['route' => ['fields.destroy', $field->id], 'method' => 'POST', 'class' => 'inline']) }}
								{{ Form::hidden('_method', 'DELETE') }}
								{{ form::submit('Delete', ['class'=>'btn btn-danger ']) }}
								{{ Form::close() }}
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>	

		<div class="form-inline form-group">
			<input type="text" id="field-name" class="form-control" placeholder="Name">
			<input type="hidden" id="field-type-id" class="form-control" value="{{ $type->id }}">
			<button class="btn btn-success" id="field-add">Add</button>
		</div>


	</div>
</div>

@endsection