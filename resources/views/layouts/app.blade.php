<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Inlusion</title>

	<script>
		var TOKEN = "{{ csrf_token() }}";
		var TYPE_ADD_URL = "{{route('types.store')}}";
		var BLOCK_ADD_URL = "{{route('blocks.store')}}";
		var FIELD_ADD_URL = "{{route('fields.store')}}";

	</script>


	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">

	<script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js" defer></script>
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" defer></script>
	<script type="text/javascript" src="{{ asset('js/ajax.js') }}" defer></script>
	
	</head>

	<body>

		<!-- Navigation -->
		<nav class="navbar navbar-inverse" role="navigation">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="{{route('index')}}">Inlusion</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li class="{{ Route::getFacadeRoot()->current()->uri() == 'types' ? 'active' : '' }}">
							<a href="{{route('types.index')}}">Types</a>
						</li>
						<li class="{{ Route::getFacadeRoot()->current()->uri() == 'blocks' ? 'active' : '' }}">
							<a href="{{route('blocks.index')}}">Blocks</a>
						</li>
					</ul>

				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container -->
		</nav>


		@yield('content')

<!-- Footer -->

    
</body>

</html>