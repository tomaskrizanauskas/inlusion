@extends("layouts.app")
@section("content")

<div class="container">
	<div class="row">
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		
		<h1>Types</h1>                                                                                   
		<div class="table-responsive">          
			<table class="table">
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@foreach($types as $type)
						<tr>
							<td>{{ $type->id }}</td>
							<td>{{ $type->name }}</td>
							<td>
								<a class="btn btn-primary" href="{{ route('fields.show', $type->id) }}">Fields</a>
								{{ Form::open(['route' => ['types.destroy', $type->id], 'method' => 'POST', 'class' => 'inline']) }}
								{{ Form::hidden('_method', 'DELETE') }}
								{{ form::submit('Delete', ['class'=>'btn btn-danger ']) }}
								{{ Form::close() }}
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>	

		<div class="form-inline form-group">
			<input type="text" id="type-name" class="form-control" name="name" placeholder="Name">
			<button class="btn btn-success" id="type-add">Add</button>
		</div>

	</div>
</div>

@endsection