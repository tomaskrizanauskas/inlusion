<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;


class BlockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $types = \App\Type::all();
        $blocks = \App\Block::all();
        
        $selectTypes = [];

        foreach ($types as $type) {
            $selectTypes[$type->id] = $type->name;
        }
        return view('blocks.index', compact('selectTypes','blocks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        'name' => 'min:1|max:50|required',
        'type_id' => 'required|exists:types,id|integer'
        ]);

        $block = \App\Block::create($request->all()); 

        $type_name = \App\Type::find($block->type_id)->name;

        $result = [
            "name"   => $block->name,
            "id"  => $block->id,
            "type_name" => $type_name,
            "fieldUrl" => route('blocks.edit', $block->id),
            "deleteUrl" => route('blocks.destroy', $block->id),
        ];

        return $result;

        // return redirect()->route('blocks.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $block = \App\Block::find($id);

        return view('blocks.edit', compact('block'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request, [
        'name' => 'required|string|min:1|max:50',
        ]);

        $block = \App\Block::find($id)->update($request->all());
        return redirect()->route('blocks.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Block::find($id)->delete();
        return redirect()->route('blocks.index');
    }
}
