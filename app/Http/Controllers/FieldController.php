<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class FieldController extends Controller
{

    public function store(Request $request)
    {
       $this->validate($request, [
        'name' => 'min:1|max:50|required',
        'type_id' => 'required|exists:types,id|integer'
        ]);

        $field = \App\Field::create($request->all()); 

        $result = [
            "name"   => $field->name,
            "id"  => $field->id,
            "deleteUrl" => route('fields.destroy', $field->id),
        ];

        return $result;

    }

    public function show($id)
    {

        $fields = \App\Field::where('type_id', $id)->get();

        $type = \App\Type::find($id);

        return view('fields.index', compact('fields','type'));
    }

    public function destroy($id)
    {   
        $typeid = \App\Field::find($id)->type_id;
        \App\Field::find($id)->delete();
        return redirect()->route('fields.show', $typeid);
    }
}
