<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $fillable = ['name'];

  	public function blocks()
    {

        return $this->hasMany('App\Block');
    }

    public function types()
    {

        return $this->hasMany('App\Type');
    }
}
