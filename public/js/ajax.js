"use strict";

$().ready(function(){

	$('#type-add').on('click', function(){

		var name = $('#type-name').val();

		$.ajax({
			method: 'POST',
			data: {
				name: name,
				_token: TOKEN
			},
			url: TYPE_ADD_URL,

			success: function(response){

				var name = response.name;
				var id = response.id;
				var fieldUrl = response.fieldUrl;
				var deleteUrl = response.deleteUrl;


				var type = '<tr><td>' + id + '</td><td>' + name + '</td>';
				type 	+= '<td><a class="btn btn-primary" href="' + fieldUrl +'">Fields</a> ';
				type 	+= '<form class="inline" method="POST" action="' + deleteUrl + '">';
				type 	+= '<input type="hidden" name="_token" value="' + TOKEN +'">';
				type 	+= '<input type="hidden" name="_method" value="DELETE">';
				type 	+= '<input type="submit" class="btn btn-danger" value="Delete">';
				type 	+= '</form></td></tr>';

					$('tbody').append(type);
					$('#type-name').val(' ');
				},
   			error : function(xhr) {
                console.log('error');
    }  
			});

		return false;
	});

	$('#block-add').on('click', function(){

		var name = $('#block-name').val();
		var id = $('#type-id').val();

		console.log(name, id);

		$.ajax({
			method: 'POST',
			data: {
				name: name,
				_token: TOKEN,
				type_id: id

			},
			url: BLOCK_ADD_URL,

			success: function(response){

				var name = response.name;
				var id = response.id;
				var type_name = response.type_name;
				var editUrl = response.fieldUrl;
				var deleteUrl = response.deleteUrl;


				var block = '<tr><td>' + id + '</td><td>' + name + '</td>';
				block 	+= '<td>' + type_name + '</td>';
				block 	+= '<td><a class="btn btn-primary" href="' + editUrl +'">Edit</a> ';
				block 	+= '<form class="inline" method="POST" action="' + deleteUrl + '">';
				block 	+= '<input type="hidden" name="_token" value="' + TOKEN +'">';
				block 	+= '<input type="hidden" name="_method" value="DELETE">';
				block 	+= '<input type="submit" class="btn btn-danger" value="Delete">';
				block 	+= '</form></td></tr>';

					$('tbody').append(block);
					$('#block-name').val(' ');
				},
   			error : function(xhr) {
                console.log('error');
    }  
			});

		return false;
	});

	$('#field-add').on('click', function(){

		var name = $('#field-name').val();
		var id = $('#field-type-id').val();

		console.log(name, id);

		$.ajax({
			method: 'POST',
			data: {
				name: name,
				_token: TOKEN,
				type_id: id

			},
			url: FIELD_ADD_URL,

			success: function(response){

				var name = response.name;
				var id = response.id;
				var type_name = response.type_name;
				var editUrl = response.fieldUrl;
				var deleteUrl = response.deleteUrl;


				var field = '<tr><td>' + id + '</td><td>' + name + '</td>';
				field 	+= '<td><form class="inline" method="POST" action="' + deleteUrl + '">';
				field 	+= '<input type="hidden" name="_token" value="' + TOKEN +'">';
				field 	+= '<input type="hidden" name="_method" value="DELETE">';
				field 	+= '<input type="submit" class="btn btn-danger" value="Delete">';
				field 	+= '</form></td></tr>';

					$('tbody').append(field);
					$('#field-name').val(' ');
				},
   			error : function(xhr) {
                console.log('error');
    }  
			});

		return false;
	});




});
